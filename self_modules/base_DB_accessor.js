var sqlite3 = require('sqlite3').verbose();
var Promise = require('bluebird');

var databaseFile = require('./../conf/conf.json').database;

if(!db) {
    var db;
    db = new sqlite3.Database(databaseFile);
}

function execInsertQuery(values, table) {
    var query = 'INSERT INTO ' + table + ' VALUES (?';
    if(values instanceof Array) {
        var length = values.length - 1;
        for(var i=0;i<length;i++) {
            query += ', ?';
        }
        query += ')';
    }
    return new Promise(function(resovle, reject) {
        resovle(db.serialize(function() {
            db.run(query, values);
        }));
    });
}

function execSelectQuery(table, whereQuery) {
    var query = 'SELECT * FROM ' + table + ' ' + whereQuery;
    return new Promise(function(resovle, reject) {
        db.all(query, function(err, row) {
            if(err) reject(err);
            else resovle(row);
        });
    });
}

function execQuery(query) {
    return new Promise(function(resovle, reject) {
        db.all(query, function(err, row) {
            if(err) reject(err);
            else resovle(row);
        });
    });
}

module.exports = {
    insert : execInsertQuery,
    select : execSelectQuery,
    execQuery : execQuery
};
