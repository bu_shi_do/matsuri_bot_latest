var Log4js = require('log4js');

Log4js.configure('/home/matsuri_project/matsuri_bot/conf/log.json');

module.exports = {
    system     : Log4js.getLogger('system'),
    tweet      : Log4js.getLogger('tweet'),
    school_bus : Log4js.getLogger('school_bus'),
    kikaku     : Log4js.getLogger('event'),
    shop       : Log4js.getLogger('shop'),
    reply      : Log4js.getLogger('reply')
};
