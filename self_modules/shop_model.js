var Promise = require('bluebird');
var _ = require('lodash');
var db = require('./base_DB_accessor.js');

//db.initDb('./../matsuri_db/development.sqlite3');

module.exports = function(str){
//shop_search = function(str){
  var goods = db.execQuery("SELECT shop_id FROM exhibits WHERE good_id in (SELECT good_id FROM good_anothers WHERE '"+str+"' LIKE '%'||good_anothers.name||'%')")
  .then(function(value){
    return new Promise.all(value.map(function(data){
      return Promise.resolve(data.shop_id);
    }));
  })
  .then(function(value){
	  return db.execQuery("SELECT * FROM shops WHERE shops.id in ("+value.toString()+")");
  });

	var group = db.execQuery("SELECT group_id FROM group_anothers WHERE '"+str+"' LIKE '%'||group_anothers.name||'%'") 
  .then(function(value){
    return Promise.all(value.map(function(data){
      return Promise.resolve(data.group_id);
    }));
  })
  .then(function(value){
	  return db.execQuery("SELECT * FROM shops WHERE shops.group_id in ("+value.toString()+")");
  });

	var name = db.execQuery("SELECT group_id FROM shops WHERE '"+str+"' LIKE '%'||shops.name||'%'")
  .then(function(value){
    return Promise.all(value.map(function(data){
      return Promise.resolve(data.group_id);
    }));
  })
  .then(function(value){
	  return db.execQuery("SELECT * FROM shops WHERE shops.group_id in ("+value.toString()+")");
  });

  return Promise.all([goods,group,name]).then(function(value){
    var result = Array.prototype.concat.apply([],value);
    return create_shop(result);
  });
}

create_shop = function(sql_data){
	return Promise.all(sql_data.map(function(data){
		return new Promise(function(resolve, reject){
			var sql_shop = db.execQuery("SELECT name FROM goods WHERE id in (SELECT good_id FROM exhibits WHERE "+data.id+" = shop_id)");
			var sql_group = db.execQuery("SELECT name FROM groups WHERE "+data.group_id+" = id");
			Promise.all([sql_shop,sql_group]).then(function(value){
				var j = {};
				j.name = data.name;
				j.booth = data.booth;
				j.group = value[1][0].name;
				j.goods = [];
				value[0].forEach(function(goods){
					j.goods.push(goods.name);
				});
				resolve(j);
			});			
		});
	}));
}

/*
var str = "C3";
shop_search(str).then(function(result){
  console.log(result);
});
*/
