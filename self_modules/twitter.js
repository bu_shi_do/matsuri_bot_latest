var twitter = require('twitter');
var Promise = require('bluebird');
var _       = require('lodash');

var logger  = require('./logger.js').tweet;

var key = require('./../json/bot_key.json');

var bot = new twitter({
	consumer_key 		:key.consumerKey,
	consumer_secret 	:key.consumerSecret,
	access_token_key	:key.accessTokenKey,
	access_token_secret :key.accessTokenSecret
});

var topicTable = {};

function tw(content) {
	bot.updateStatus(content, function(data) {
		//console.log(data);
        logger.info(data.text);
	});
}

function reply(content, sourceData) {
	sourceTweetId = sourceData.id_str;
	toReplyUser = sourceData.user.screen_name;
	content = "@" + toReplyUser + " " +content;
    logger.info(content);

	return new Promise(function(resolve, reject) {
        var tweetedData;
        bot.updateStatus(content,{in_reply_to_status_id:sourceTweetId}, function(data) {
            resolve(data);
	    });
    });
}

function getReplyStatusId(status_id) {
    return bot.showStatus(status_id, function(tweetObj) {
        return tweetObj.in_reply_to_status_id;
    });
}

function getTopic(status_id) {
    return topicTable[String(status_id)];
}

function setTopic(status_id, topic) {
    topicTable[String(status_id)] =  _.cloneDeep(topic);
}

function deleteTopic(status_id) {
    topicTable[String(status_id)] = null;
}
    
module.exports = {
	tw : tw,
	reply : reply,
    getReplyStatusId : getReplyStatusId,
    getTopic : getTopic,
    setTopic : setTopic,
    deleteTopic : deleteTopic
}
