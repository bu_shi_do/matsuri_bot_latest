var db = require('./base_DB_accessor.js');
var Promise = require('bluebird');

//db.initDb('./../matsuri_db/development.sqlite3');

module.exports = function(){
//chat_search = function(){
  return db.execQuery("SELECT regword,word FROM chats")
  .then(function(value){
    return Promise.all(value.map(function(data){
      return Promise.resolve(data);
    }));
  });
}

/*
chat_search().then(function(result){
  console.log(result);
});
*/

