var db = require('./base_DB_accessor.js');
var Promise = require('bluebird');

//db.initDb('./../matsuri_db/development.sqlite3');

var location = [0];
db.execQuery('SELECT id, name FROM locations')
.then(function(value){
	value.forEach(function(data){
		location.push(data.name);
	});
})
.catch(function(e) {
    console.log(e);
});

function create_project(sql_data){
	  return Promise.all(sql_data.map(function(data){
		  var j = {
			  title: data.title,
  			location: location[data.location_id],
	  		introduction: data.introduction,
		  	interval_flag: false,
			  interval: data.tweet_interval,
  			edited_flag: false,
	  	}
		  var start_time = new Date(data.start_time);
  		var end_time = new Date(data.end_time);
	  	if(!!data.edited_start_time){
		  	j.edited_flag = true;
			  start_time = new Date(data.edited_start_time);
  			end_time = new Date(data.edited_end_time);
	  	}
		  j.start = start_time;
  		j.end = end_time;
	  	if(j.interval !== 0){
		  	j.interval_flag = true;
  		}
	  	return Promise.resolve(j);
    }));
/*
  j = {
    title:          string,
    location:       string,
    introduction:   string,
    start:          Date,
    end:            Date,
    interval_flag:  boolean,
    interval:       int,
    edited_flag:    boolean
  }
*/
}

function search_string(str){
  return db.execQuery('SELECT id, keyword FROM projects')
  .then(function(value){
    return new Promise(function(resolve,reject){
      var project = {};
	    value.forEach(function(data){
		    project[data.id] = data.keyword.split(/\r\n|\r|\n/);
  	  });
      resolve(project);
    });
  }).then(function(value){
    return new Promise(function(resolve,reject){
      var ids = [];
      for(id in value){
        value[id].forEach(function(keyword){
          if(~str.indexOf(keyword)){
            ids.push(id);
          }    
        });
      }
      resolve(ids);
    });
  }).then(function(ids){
    var sql = "SELECT * FROM projects WHERE id in ("+ids+")"
    return db.execQuery(sql);
  }).then(function(value){
    return create_project(value);
  });
}

function search_date(){

}

module.exports = {
  search_string : search_string,
  search_date: search_date
}
/*
var str = "巨大";
search_string(str).then(function(result){
  console.log(result);
});
*/
