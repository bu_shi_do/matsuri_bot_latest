var redis = require('redis');
var promise = require('bluebird');

var conf = require('../config/redis.js');

client = redis.createClient();

client.on('error', function(err) {
    console.log("ERR: "+err);
});

module.exports = {
    set : function(table, key, val) {
        return new Promise(function(resolve, reject) {
            if(client.set(table+"_"+key, val)) {
                resolve(true);
            } else {
                reject(new Error("Setting fault");
            }
        });
    }
}

