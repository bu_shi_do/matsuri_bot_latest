var db = require('./base_DB_accessor.js');
var Promise = require('bluebird');

//db.initDb('./../matsuri_db/development.sqlite3');

var location = [0];
db.execQuery('SELECT id, name FROM locations')
.then(function(value){
	value.forEach(function(data){
		location.push(data.name);
	});
})
.catch(function(e) {
    console.log(e);
});

var group = [0];
db.execQuery('SELECT id, name FROM groups')
.then(function(value){
	value.forEach(function(data){
		group.push(data.name);
	});
})
.catch(function(e) {
    console.log(e);
});

function create_club(sql_data){
	return Promise.all(sql_data.map(function(data){
		var j = {
			name: data.name,
      group_name: group[data.group_id],
			location: location[data.location_id],
			introduction: data.introduction,
		  start: new Date(data.start_time),
		  end: new Date(data.end_time)
		}
		return Promise.resolve(j);
	}));
/*
  j = {
    name:           string,
    group_name:     string,
    location:       string,
    introduction:   string,
    start:          Date,
    end:            Date
  }
*/
}

function search_string(str){
  var key = db.execQuery('SELECT id, keyword FROM clubs')
  .then(function(value){
    return new Promise(function(resolve,reject){
      var club = {};
	    value.forEach(function(data){
		    club[data.id] = data.keyword.split(/\r\n|\r|\n/);
  	  });
      resolve(club);
    });
  }).then(function(value){
    return new Promise(function(resolve,reject){
      var ids = [];
      for(id in value){
        value[id].forEach(function(keyword){
          if(~str.indexOf(keyword)){
            ids.push(id);
          }    
        });
      }
      resolve(ids);
    });
  }).then(function(ids){
    return db.execQuery("SELECT * FROM clubs WHERE id in ("+ids+")");
  });
  var name = db.execQuery("SELECT group_id FROM group_anothers WHERE '"+str+"' LIKE '%'||group_anothers.name||'%'")
  .then(function(value){
    return Promise.all(value.map(function(data){
        return Promise.resolve(data.group_id);
    }));
  }).then(function(value){
    return db.execQuery("SELECT * FROM clubs WHERE clubs.group_id in ("+value.toString()+")");
  });
  
  return Promise.all([key,name]).then(function(value){
    var result = Array.prototype.concat.apply([],value);
    return create_club(result);
  });
}

function search_date(){

}

module.exports = {
  search_string : search_string,
  search_date: search_date
}

/*
var str = "C3";
search_string(str).then(function(result){
  console.log(result);
});
*/
