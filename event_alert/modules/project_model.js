var _ = require('lodash');

var db = require('../../self_modules/base_DB_accessor.js');
var logger = require('../../self_modules/logger.js').system;

var projects = [];
var loc     = [];

//db.initDb('/home/matsuri_project/matsuri_db/test.db');

db.select('locations', '')
.then(function(rows) {
    _.map(rows, function(row) {
        loc[row.id] = row.name;
    });
})
.catch(function(e) {
    logger.error(e);
});

db.select('projects', '')
.then(function(rows) {
    _.map(rows, function(row) {
        projects[row.id] = row;
    });
})
.catch(function(e) {
    logger.error(e);
});

function getProject(referTime) {
    return projects[referTime.getTime()];
}

function getNextProject(referTime) {
    var referUnixTime = referTime.getTime();
    var nextProjects = [];
    for(var i=1;i<projects.length;i++) {
        var startTime = new Date(projects[i].start_time).getTime();
        if(startTime < referUnixTime) continue;
        nextProjects.push(projects[i]);
        i++;
        while(startTime === new Date(projects[i].start_time).getTime()) {
            nextProjects.push(projects[i]);
            i++;
        }
        return nextProjects;
    } 
}

function getAllProjects() {
    return projects;
}

function getLocation(id) {
    return loc[id];
}

function getAllLocation() {
    return loc;
}

module.exports = {
    getProject     : getProject,
    getAllProjects : getAllProjects,
    getNextProject : getNextProject,
    getLocation    : getLocation,
    getAllLocation : getAllLocation
};
