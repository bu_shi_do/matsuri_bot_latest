var _ = require('lodash');

var logger = require('./../self_modules/logger.js').kikaku;
var content = require('./../json/event_alert_content.json');
var db = require('./modules/project_model.js');
var tweetutl = require('./../self_modules/twitter.js');
var formatNumber = require('./modules/formatNumber.js');

function formatTime(min) {
    var tmpHour = Math.floor(min / 60);
    var tmpMin  = min % 60;
    if(tmpHour === 0) {
        return formatNumber(tmpMin)+content.insessionTime[1];
	} else if(tmpMin === 0) {
		return formatNumber(tmpHour)+content.insessionTime[0];
    } else {
        return formatNumber(tmpHour)+content.insessionTime[0]+formatNumber(tmpMin)+content.insessionTime[1];
    }
}

function selectProject (referTime) {
	if(referTime.getSeconds() !== 0) return;
    var projects = db.getAllProjects();
    if(projects.length === 0) return;
    var now = new Date();

    logger.info('routine kikaku is alive');

    _.map(projects, function(project) {
        if(project === undefined) return;
        var projectStartTime = new Date((!project.edited_start_time)? project.start_time : project.edited_start_time);
        if(now.getDate() !== projectStartTime.getDate()) return;
        var restHour = projectStartTime.getHours() - now.getHours();
        var restMinute = projectStartTime.getMinutes() - now.getMinutes() + restHour * 60;
        var k = 0;

        //started project
        if(project.tweet_interval !== 0) {
            var projectEndTime = new Date((!project.edited_end_time)? project.end_time : project.edited_end_time);
            var restEndHour = projectEndTime.getHours() - now.getHours();
            var restEndMinute = projectEndTime.getMinutes() - now.getMinutes() + restEndHour * 60;
            if(projectStartTime < referTime && referTime < projectEndTime) {
                if(restEndMinute % (project.tweet_interval * 60) === 0) {
                    k = 0;
                    var tweetText = db.getLocation(project.location_id)
                        + content.insessionText[k++]
                        + project.title
                        + content.insessionText[k++]
                        + content.insessionText[k++]
                        + formatTime(restEndMinute)
                        + content.insessionText[k++]
                        + content.insessionText[k];
                    tweetutl.tw(tweetText);
//                    console.log(tweetText);
                }
            }
        }

        //start just now
        if(restMinute === 0) {
            var tweetText = db.getLocation(project.location_id)
                          + content.startText[k++]
                          + project.title
                          + content.startText[k++]
                          + content.startText[k++];
            tweetutl.tw(tweetText);
        }

        //before start project
        for(var i=0;i<content.valiableText.beforemins.length;i++) {
            if(restMinute === content.valiableText.beforemins[i]) {
                var tweetText = projectStartTime.getHours()
                    + content.regularText[k++]
                    + projectStartTime.getMinutes()
                    + content.regularText[k++]
                    + content.regularText[k++]
                    + db.getLocation(project.location_id)
                    + content.regularText[k++]
                    + project.title
                    + content.regularText[k++]
                    + content.regularText[k++];
                    tweetutl.tw(tweetText);
//                    console.log(tweetText);
                    break;
            }

        }


	});

}

process.on("message", function(msg) {
    if(msg.message === 'stop') process.exit(0);
});

setInterval(function() {
	var dd = new Date();
	selectProject(dd);
}, 1000);
