var Promise = require('bluebird');
var shop_model = require('./../../self_modules/shop_model');

function make_tweet_str(data){
  return new Promise(function(resolve,reject){
    var str = (data.group+"さんの「"+data.name+"」は"+data.booth+"番にあります。\n");
    data.goods.forEach(function(d){
      str += (d+",");
    });
    str = str.substr(0,(str.length-1));
    str += "を出店しています。"
    resolve(str);
  });
}

module.exports = function(data){
//s = function(data){
  var str = data.text;
  return shop_model(str)
  .then(function(value){
    return Promise.all(value.map(function(data){
      return make_tweet_str(data);
    }));
  });
}

/*
var data = {text:"C3"};
s(data).then(function(result){
  console.log(result);
});
*/
