var Promise = require('bluebird');
var specific_model = require('./../../self_modules/specific_model.js');

module.exports = function(data){
  var str = data.text;
  return specific_model(str);
}
