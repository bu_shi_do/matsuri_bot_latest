module.exports = function(tweetStr, callback){
	var overlap = tweetStr.filter(function (x, i, self) {
            return self.indexOf(x) === i && i !== self.lastIndexOf(x);
        });
	if(overlap > 0)	callback(null, overlap);
	else	callback(null, tweetStr);
};

