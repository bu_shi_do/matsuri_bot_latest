var Promise = require('bluebird');
var club_model = require('./../../self_modules/club_model.js');
var format_date = require('./../../self_modules/format_date.js');

function make_tweet_str(data){
  return new Promise(function(resolve,reject){
    var now = new Date();
    var start = new Date(data.start);
    var end = new Date(data.end);
    var format = "DD日hh時mm分";
    var str = (data.group_name+"さんの");
    if(data.group_name === data.name){
      str += ("サークル展示は");
    }else{
      str += (data.name+"は");
    }
    if(start.getTime() > now.getTime()){
      str += (data.location+"で行っています。\n"+format_date(start,format)+"から開催されます。\n"+data.introduction);
    }else if(end.getTime() > now.getTime()){
      str += (data.location+"で行っています。\n"+format_date(end,format)+"まで開催されます。\n"+data.introduction);
    }else{
      str += (format_date(end,format)+"をもって終了しました。");
    }
    resolve(str);
  });
}

module.exports = function(data){
//c = function(data){
  var str = data.text;
  return club_model.search_string(str)
  .then(function(value){
    return Promise.all(value.map(function(data){
      return make_tweet_str(data);
    }));
  });
}

/*
var data = {text:"アニ研"};
c(data).then(function(result){
  console.log(result);
});
*/

