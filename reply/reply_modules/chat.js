var Promise = require('bluebird');
var chat_model = require('./../../self_modules/chat_model.js');

module.exports = function(data){
//c = function(data){
  var str = data.text;
  return chat_model()
  .then(function(value){
    return Promise.all(value.map(function(words){
      var re = new RegExp(words.regword, 'g');
      if(str.match(re)){
        return Promise.resolve(words.word);
      }
      return Promise.resolve("");
    }));
  }).then(function(value){
    return new Promise(function(resolve,reject){
      resolve(value.filter(function(e){return !!e}));
    });
  });
}

/*
var data = {text: "祭ちゃん"};
c(data).then(function(result){
  console.log(result);
});
*/

