var Promise = require('bluebird');
var project_model = require('./../../self_modules/project_model.js');
var format_date = require('./../../self_modules/format_date.js');

function make_tweet_str(data){
  return new Promise(function(resolve,reject){
    var now = new Date();
    var start = new Date(data.start);
    var end = new Date(data.end);
    var str = "";
    var format = "DD日hh時mm分"
    if(start.getTime() > now.getTime()){
      str += (format_date(start,format)+"より"+data.location+"にて"+data.title+"が開催されます。\n"+data.introduction);
    }else if(end.getTime() > now.getTime()){
      str += (format_date(start,format)+"より"+data.location+"にて"+data.title+"が開催されています。\n"+data.introduction);
    }else{
      str += (format_date(end,format)+"をもって"+data.title+"は終了しました。\n"+data.introduction);
    }
	  if(data.edited_flag){
		  str += ("\nこの企画は時間が変更になっていますので気を付けてください。");
  	}
    resolve(str);
  });
}

module.exports = function(data){
//p = function(data){
  var str = data.text;
  return project_model.search_string(str)
  .then(function(value){
    return Promise.all(value.map(function(data){
      return make_tweet_str(data);
    }));
  });
}

/*
var data = {text:"巨大"};
p(data).then(function(result){
  console.log(result);
});
*/

