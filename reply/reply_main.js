var twitter = require('twitter');
var Promise = require('bluebird');
var tw = require('./../self_modules/twitter');
var format_date = require('./../self_modules/format_date');
var logger = require('./../self_modules/logger').reply;
var key = require('./../json/bot_key.json');
var shop = require('./reply_modules/shop');
var club = require('./reply_modules/club');
var project = require("./reply_modules/project");
var overlap = require("./reply_modules/overlap");
var specific = require('./reply_modules/specific');
var chat = require('./reply_modules/chat');
var auto_reply = require('./../school_bus_bot/auto_reply_kai');

const BOT_ID = 310781015;
var LOGFILE = "reply_main_log";

var bot = new twitter({
	consumer_key 		:key.consumerKey,
	consumer_secret 	:key.consumerSecret,
	access_token_key	:key.accessTokenKey,
	access_token_secret :key.accessTokenSecret
});

process.on("message", function(msg) {
    if(msg.message === 'stop') process.exit(0);
});

logger.info('bot started');

bot.stream('user', function(stream){
	stream.on('data', function(data){
		if(!('text' in data)){ return; }
		var screen_name = data.user.screen_name;
		var user_name = data.user.name;
		var user_id = data.user.id_str;
		var twtext = data.text;
		var twdate = new Date(data.created_at);

		var isMention = data.entities.user_mentions.length ? data.entities.user_mentions[0].id_str == BOT_ID : false;	// 自分へのreplyかどうか
		var isRetweet = data.retweeted_status ? true : false;	// 公式RTかどうか
		var isRetweet2 = (new RegExp(/(R|Q)T @[^\s　]+/g)).test(twtext);	// 非公式RT,QTかどうか
		var isLink = data.entities.urls.length ? true : false;	// リンクを含むかどうか

		twtext = twtext.replace(new RegExp(/@[^\s　]+/g),'')	// @aaaaaは全て除外
		twtext = twtext.replace(/[\n\r]/g,"");	//改行を除去

		/* 自分自身のツイートは除外 */
		if(user_id == BOT_ID){ return; }
		/* リンク付きとRTは除外 */
		if(isRetweet || isRetweet2 || isLink){ return; }

		auto_reply.auto_reply(data);

		if(isMention){
			tmp = format_date(new Date(),"YYYY年MM月DD日 hh時mm分ss秒");
			logger.info(tmp);
			tmp = user_name + "<" + "@" + screen_name + ">" + " " + twtext;
			logger.info(tmp);

			var shop_promise = shop(data);
			var project_promise = project(data);
			var club_promise = club(data);
			var specific_promise = specific(data);
			var chat_promise = chat(data);
			var promise_array = [shop_promise,project_promise,club_promise,specific_promise,chat_promise];
			Promise.all(promise_array).then(function(value){
        var a = Array.prototype.concat.apply([], value);
        var str = [];
        overlap(a, function(err,result){
        	str = result;
        });
        return str;
			}).then(function(value){
        logger.info(value);
        value.forEach(function(str){
				  tw.reply(str, data);
				});
			});
		}
	});
	stream.on('error', function(error) {
    logger.error(error);
		throw error;
	});
});
