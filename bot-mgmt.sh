#!/usr/bin/bash

CURRENTDIR=`dirname $0`

		ALIVEROUTINE=`ps ax | grep -v grep | grep "routine.js"  | wc -l`
		ALIVEEVENTAL=`ps ax | grep -v grep | grep "event_alert.js"  | wc -l`
		ALIVEMAIN=`ps ax | grep -v grep | grep "reply_main.js"  | wc -l`

		if [ $ALIVEROUTINE = 0 ]; 
		then
			echo "routine.js    does not run."
		else
			echo "routine.js    running now"
		fi
		if [ $ALIVEEVENTAL = 0 ]; 
		then
			echo "event_alert.js does not run."
		else
			echo "event_alert.js is running."
		fi
		if [ $ALIVEMAIN = 0 ]; 
		then
			echo "reply_main.js does not run."
		else
			echo "reply_main.js is runnning."
		fi
