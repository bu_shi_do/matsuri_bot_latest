var _ = require('lodash');

var analyze = require('./modules/analyze.js');
var selectTime = require('./modules/select_time.js');
var content = require('./../json/content.json');
var twitter = require('./../self_modules/twitter.js');
var timeTableDb = require('./modules/schoolbus_model.js');
var logger = require('./../self_modules/logger.js').school_bus;
var fromKyutech, fromBusCenter, fromIizukaSta;
setTimeout(function() {
    fromKyutech   = timeTableDb.getTimeTable('kyutech');
    fromBusCenter = timeTableDb.getTimeTable('buscenter');
    fromIizukaSta = timeTableDb.getTimeTable('iizukasta');
}, 100);

//var BOTID = 'NTB_001';
//var BOTID = 'JSbot_Shaher';
var BOTID = 'KoudaiMatsuri';

function auto_reply(data) {
    var DD, from;
    if(!('text' in data)){return;}

    var userId = data.user.screen_name;
    var twStr = data.text;
    if(userId === BOTID) {return;}
    if(twStr.indexOf(BOTID) === -1) {return;}

    logger.info(userId + ':' + twStr);
    var topic = twitter.getTopic(data.in_reply_to_status_id);
    logger.info(topic);
    twitter.deleteTopic(data.in_reply_to_status_id);
    if(!analyze.searchBus(twStr)) {
        if(!topic) return;
    }


    if(topic) {
        from = topic.from;
        DD = topic.time;
    }

    var timetable;
    var twText;
    var k;
    if(!topic||topic.from==='undefined') from = analyze.searchFrom(twStr);
    switch (from) {
        case "kyutech":
            timetable = fromKyutech;
            break;
        case "buscenter":
            timetable = fromBusCenter;
            break;
        case "iizukasta":
            timetable = fromIizukaSta;
            break;
        default:
            twText = content.UnknownText;
            DD = new Date();
            if(analyze.searchNext(twStr)!== -1) {
            } else {
                inputTime = analyze.searchTime(twStr);
                if(inputTime === -1) break;
                if(DD.getHours() > 12 && inputTime.getHours() < 12) 
                    inputTime.setHours(inputTime.getHours() + 12);
                DD = _.cloneDeep(inputTime);
            }
            twitter.reply(twText, data)
            .then(function(tweetedData) {
                twitter.setTopic(tweetedData.id, {
                    from : 'undefined',
                    time : DD});
            });
            return;
    }       

    //next
    if(analyze.searchNext(twStr) !== -1) {
        if(!topic) DD = new Date();
        var returnedtime = _.cloneDeep(selectTime.selectFromTimeTable(DD.getTime(), timetable.time));
        if(returnedtime.getTime() === 0) {
            if(new Date().getDate()===22) {
                twText = content.AfterLastBus;
                twitter.reply(twText, data);
                return;
            } else {
                returnedtime = new Date(timetable[0]);
            }
        }
        k = 0;
        twText = content.replyText[k++] 
            + timetable.from
            + content.replyText[k++]
            + returnedtime.getHours()
            + content.replyText[k++]
            + returnedtime.getMinutes()
            + content.replyText[k++];
            twitter.reply(twText, data)
            .then(function(tweetedData) {
                var topicTime = new Date(returnedtime.getTime() + 60000);
                twitter.setTopic(tweetedData.id, {
                    from : from,
                    time : topicTime});
            });
    }

    var inputTime = analyze.searchTime(twStr);	
    if(inputTime !== -1) {
        DD = new Date();
        if(DD.getHours() > 12 && inputTime.getHours() < 12) 
            inputTime.setHours(inputTime.getHours() + 12);
        var returnedtime = _.cloneDeep(selectTime.selectFromTimeTable(inputTime.getTime(), timetable.time));
        k = 4;
        twText = content.replyText[k++] 
            + timetable.from
            + content.replyText[k++]
            + returnedtime.getHours()
            + content.replyText[k++]
            + returnedtime.getMinutes()
            + content.replyText[k++];
            twitter.reply(twText, data)
            .then(function(tweetedData) {
                var topicTime = new Date(returnedtime.getTime() + 60000);
                twitter.setTopic(tweetedData.in_reply_to_status_id, {
                    from : from,
                    time : topicTime });
            });
    }

    //no info of time and next
    if(analyze.searchNext(twStr) === -1 && inputTime === -1) {
        twText = content.NoFromOrTimeText;
        twitter.reply(twText, data);
        return;
    }

    DD = new Date();
    var returnedtime = selectTime.selectFromTimeTable(DD.getTime(), timetable.time);
    if(returnedtime.getTime() === 0) {
        twText = content.AfterLastBus;
        twitter.reply(twText, data);
        return;
    }
}

module.exports = {
    auto_reply : auto_reply
};
