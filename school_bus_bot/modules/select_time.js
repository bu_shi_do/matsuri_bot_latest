var timeUtil = require('./timeUtil.js');

function selectFromTimeTable(referTime, timeTable) {
    var i = 0;
    while(timeUtil.subTime(new Date(referTime), new Date(timeTable[i])).getTime() > 0 && i < timeTable.length) {
        i++;
    }
    if(i === timeTable.length) {
        return new Date(0);
    }
    return new Date(timeTable[i]);
}

module.exports = {
	selectFromTimeTable : selectFromTimeTable
};
