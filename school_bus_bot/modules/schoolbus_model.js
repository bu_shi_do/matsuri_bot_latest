var _ = require('lodash');

var db = require('../../self_modules/base_DB_accessor.js');

var timeTable = {kyutech : {}, iizukasta : {}, buscenter : {}};

//db.initDb('test/test_data/school_bus.db');

db.select('bus_stops', '')
.then(function(rows) {
    timeTable.kyutech.from = rows[0].name;
    timeTable.buscenter.from = rows[1].name;
    timeTable.iizukasta.from = rows[2].name;
});

db.select('bus_timetables', '')
.then(function(rows) {
   timeTable.kyutech.time   = _.pull(_.pluck(_.filter(rows, {'bus_stop_id' : 1}), 'depart_time'), 0);
   timeTable.buscenter.time = _.pull(_.pluck(_.filter(rows, {'bus_stop_id' : 2}), 'depart_time'), 0);
   timeTable.iizukasta.time = _.pull(_.pluck(_.filter(rows, {'bus_stop_id' : 3}), 'depart_time'), 0);
})
.then(function() {
    timeTable.kyutech.time = _.map(timeTable.kyutech.time, function(time) {
                                return new Date('2000/01/01 ' + String(time));
                             });
    timeTable.buscenter.time = _.map(timeTable.buscenter.time, function(time) {
                                return new Date('2000/01/01 ' + String(time));
                             });
    timeTable.iizukasta.time = _.map(timeTable.iizukasta.time, function(time) {
                                return new Date('2000/01/01 ' + String(time));
                             });
})
.catch(function(e) {
    console.log(e);
});

function getTimeTable(from) {
    return timeTable[from];
}

module.exports = {
    getTimeTable : getTimeTable
};
