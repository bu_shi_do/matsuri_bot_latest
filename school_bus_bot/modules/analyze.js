var searchStr = require('./../../json/str.json');
var flag;
var pos;

function searchNext(text) {
	var exist = 0;

	for(i=0;i<searchStr.next.length;i++) {
		exist = text.indexOf(searchStr.next[i]);
		if(exist != -1) break;
	}
	
	return exist;
}

function searchNumber(text) {
	var FWNumbers = searchStr.NUMBER;
    var Kansuzi = searchStr.KANSUZI;

	for(i = 0;i < 10;i++) {
		if(text == i.toString()) return i;
	}

	for(i = 0;i < FWNumbers.length;i++) {
		if(text == FWNumbers[i]) return i;
	}

    for(i = 0;i < Kansuzi.length;i++) {
        if(text == Kansuzi[i] && i == 10) return 1;
        if(text == Kansuzi[i] ) return i;
    }
    if(Kansuzi[10] == text) return 0;
	
    return -1;
}	

function searchHourWord(text) {
	var exist = 0;
	var hour = 0;
    flag = false;

	for(i=0;i<searchStr.zi.length;i++) {
		exist = text.indexOf(searchStr.zi[i]);
        pos = exist;
		if(exist != -1) {
            if(i >= 2) flag = true;
			hour = searchNumber(text[exist-1]);
			if(searchNumber(text[exist-2]) != -1) {
                hour = hour + searchNumber(text[exist-2]) * 10;
			}
			return hour;
		}
	}	
	return -1;
}

function searchMinWord(text) {
	var exist = 0;
	var min = 0;

    if(flag) {
        min = searchNumber(text[pos+1]);
        if(min != -1) {
            if(searchNumber(text[pos+2] != -1)) {
                min = min * 10 + searchNumber(text[pos+2]);
            }
            return min;
        }
    }

	for(i=0;i<searchStr.hun.length;i++) {
		exist = text.indexOf(searchStr.hun[i]);
		if(exist != -1) {
			min = searchNumber(text[exist-1]);
			if(searchNumber(text[exist-2] != -1)) {
				min = min + searchNumber(text[exist-2]) * 10; 
			}
			return min;
		}
	}	

	for(i=0;i<searchStr.han.length;i++) {
		exist = text.indexOf(searchStr.han[i]);
		if(exist != -1) return 30;
	}
    /*if(flag) {
        min = searchNumber(text[pos+1]);
        if(min != -1) {
            if(searchNumber(text[pos+2] != -1)) {
                min = min + searchNumber(text[pos+2]) * 10;
            }
            return min;
        }
    }*/

	return -1;
}

function searchTime(text) {
    var hour = searchHourWord(text);
    if(hour===-1) return -1;
    var minute  = searchMinWord(text);
    if(minute === -1) minute = 0;
    return new Date((hour - 9) * 3600000 + minute * 60000);
}

function searchFrom(text) {
    for(i=0;i<searchStr.FROM.length;i++) {
        fromLocation = searchStr.FROM[i];
        for(j=0;j<fromLocation.LOCALESTR.length;j++) {
            exist = text.indexOf(fromLocation.LOCALESTR[j]);
            if(exist != -1) return fromLocation.LOCALE;
        }
    }
}

function searchBus(text) {
	for(i=0;i<searchStr.BUS.length;i++) {
		exist = text.indexOf(searchStr.BUS[i]);
		if(exist != -1) {
			return true;
		} else {
			return false;
		}
	}
}

module.exports = {
	searchNext : searchNext,
	searchTime : searchTime,
    searchFrom : searchFrom,
	searchBus  : searchBus
}
