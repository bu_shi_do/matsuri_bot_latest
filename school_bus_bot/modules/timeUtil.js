function subTime(drawn, draw) {
    var hour = drawn.getHours();
    var min  = drawn.getMinutes();
    var sec  = drawn.getSeconds();

    drawn = new Date(0);
    drawn.setFullYear(2000);
    drawn.setHours(hour);
    drawn.setMinutes(min);
    drawn.setSeconds(sec);

    return new Date(drawn.getTime() - draw.getTime());
}

module.exports = {
    subTime : subTime
};
