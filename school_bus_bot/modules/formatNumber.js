function formatNumber(num) {
	if(num < 10) 
		num = "0" + num;
	return num;
}

module.exports = formatNumber;
