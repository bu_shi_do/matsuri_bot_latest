var selectTime = require('./modules/select_time.js');
var noticeText = require('./../json/content.json');
var twitter    = require('./../self_modules/twitter.js');
var logger     = require('./../self_modules/logger.js').school_bus;
var formatNumber = require('./modules/formatNumber');

var timeTableDb = require('./modules/schoolbus_model.js');
var referOder = [];
setTimeout(function() {
    referOder.push(timeTableDb.getTimeTable('kyutech'));
    referOder.push(timeTableDb.getTimeTable('buscenter'));
    referOder.push(timeTableDb.getTimeTable('iizukasta'));
}, 100);

logger.info("Routine started");

process.on("message", function(msg) {
    if(msg.message === 'stop') process.exit(0);
});

setInterval(function() {
    var now = new Date();
    if(now.getSeconds() !== 0) return;

    logger.info('routine alive');

    for(var i=0;i<referOder.length;i++) {
        var returnedtime = selectTime.selectFromTimeTable(now.getTime(), referOder[i].time);
        var diffMinute = (returnedtime.getMinutes() + returnedtime.getHours() * 60)
                       - (now.getMinutes() + now.getHours() * 60);
        var count = -1;
        for(var j=0;j<noticeText.valiableText.length;j++) {
            if(noticeText.valiableText[j].beformin === diffMinute) {
                count = j;
                break;
            }
        }
        if(count === -1) return;
        if(noticeText.valiableText[count].beformin === diffMinute) {
            var tweetText = returnedtime.getHours()
                + noticeText.regularText.zi 
                + formatNumber(returnedtime.getMinutes())
                + noticeText.regularText.hun 
                + referOder[i].from
                + noticeText.regularText.text 
                + noticeText.valiableText[count].content;
            twitter.tw(tweetText);
            //console.log(tweetText);
        }
    }
}, 1000);
