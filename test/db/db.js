var db = require('../../self_modules/base_DB_accessor.js');

db.initDb(process.argv[2]);

db.select('test', '')
.then(function(data) {
    console.log(data);
})
.then(function() {
    return db.insert(["3", "piyo"], 'test');
})
.then(function() {
    return db.select('test','')
    .then(function(data) {
        console.log(data);
    });
})
.catch(function(e) {
    console.log(e);
});
