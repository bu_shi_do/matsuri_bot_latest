var fs = require('fs');

var FILE = 'timtetable.sql';

var sql = "DROP TABLE IF EXISTS bus_stop;";
sql += "DROP TABLE IF EXISTS bus_timetable;\n";
sql += "CREATE TABLE bus_stop (id int, name text);\n";
sql += "CREATE TABLE bus_timetable (id int, bus_stop_id int, depart_time time);\n";
sql += "INSERT INTO bus_stop (id, name) VALUES\n";
sql += "(1, '飯塚バスセンター'),(2, '新飯塚駅'),(3, '九工大飯塚キャンパス');\n";
sql += "INSERT INTO bus_timetable (id, bus_stop_id, depart_time) VALUES\n";

var counta = 0;

for(var i=0;i<24;i++) {
    for(var j=0;j<60;j+=20) {
        sql += "("+counta+", 1, time('";
        sql += (i < 10)? '0' + String(i) : i;
        sql += ':';
        sql += (j === 0)? '00': j ;
        sql += "')),\n";
        counta++;
    }
}

sql += ";";


fs.writeFile(FILE, sql, function(error) {
    console.log(error);
});
