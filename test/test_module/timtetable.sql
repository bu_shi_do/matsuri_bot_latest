DROP TABLE IF EXISTS bus_stop;DROP TABLE IF EXISTS bus_timetable;
CREATE TABLE bus_stop (id int, name text);
CREATE TABLE bus_timetable (id int, bus_stop_id int, depart_time time);
INSERT INTO bus_stop (id, name) VALUES
(1, '飯塚バスセンター'),(2, '新飯塚駅'),(3, '九工大飯塚キャンパス');
INSERT INTO bus_timetable (id, bus_stop_id, depart_time) VALUES
(0, 1, time('00:00')),
(1, 1, time('00:20')),
(2, 1, time('00:40')),
(3, 1, time('01:00')),
(4, 1, time('01:20')),
(5, 1, time('01:40')),
(6, 1, time('02:00')),
(7, 1, time('02:20')),
(8, 1, time('02:40')),
(9, 1, time('03:00')),
(10, 1, time('03:20')),
(11, 1, time('03:40')),
(12, 1, time('04:00')),
(13, 1, time('04:20')),
(14, 1, time('04:40')),
(15, 1, time('05:00')),
(16, 1, time('05:20')),
(17, 1, time('05:40')),
(18, 1, time('06:00')),
(19, 1, time('06:20')),
(20, 1, time('06:40')),
(21, 1, time('07:00')),
(22, 1, time('07:20')),
(23, 1, time('07:40')),
(24, 1, time('08:00')),
(25, 1, time('08:20')),
(26, 1, time('08:40')),
(27, 1, time('09:00')),
(28, 1, time('09:20')),
(29, 1, time('09:40')),
(30, 1, time('10:00')),
(31, 1, time('10:20')),
(32, 1, time('10:40')),
(33, 1, time('11:00')),
(34, 1, time('11:20')),
(35, 1, time('11:40')),
(36, 1, time('12:00')),
(37, 1, time('12:20')),
(38, 1, time('12:40')),
(39, 1, time('13:00')),
(40, 1, time('13:20')),
(41, 1, time('13:40')),
(42, 1, time('14:00')),
(43, 1, time('14:20')),
(44, 1, time('14:40')),
(45, 1, time('15:00')),
(46, 1, time('15:20')),
(47, 1, time('15:40')),
(48, 1, time('16:00')),
(49, 1, time('16:20')),
(50, 1, time('16:40')),
(51, 1, time('17:00')),
(52, 1, time('17:20')),
(53, 1, time('17:40')),
(54, 1, time('18:00')),
(55, 1, time('18:20')),
(56, 1, time('18:40')),
(57, 1, time('19:00')),
(58, 1, time('19:20')),
(59, 1, time('19:40')),
(60, 1, time('20:00')),
(61, 1, time('20:20')),
(62, 1, time('20:40')),
(63, 1, time('21:00')),
(64, 1, time('21:20')),
(65, 1, time('21:40')),
(66, 1, time('22:00')),
(67, 1, time('22:20')),
(68, 1, time('22:40')),
(69, 1, time('23:00')),
(70, 1, time('23:20')),
(71, 1, time('23:40'))
;
