DROP TABLE IF EXISTS bus_stop;DROP TABLE IF EXISTS bus_timetable;
CREATE TABLE bus_stop (id int, name text);
CREATE TABLE bus_timetable (id int, bus_stop_id int, depart_time time);
INSERT INTO bus_stop (id, name) VALUES
(1, '飯塚バスセンター'),(2, '新飯塚駅'),(3, '九工大飯塚キャンパス');
INSERT INTO bus_timetable (id, bus_stop_id, depart_time) VALUES
(0, 1, time('00:00')),
(1, 1, time('00:05')),
(2, 1, time('00:10')),
(3, 1, time('00:15')),
(4, 1, time('00:20')),
(5, 1, time('00:25')),
(6, 1, time('00:30')),
(7, 1, time('00:35')),
(8, 1, time('00:40')),
(9, 1, time('00:45')),
(10, 1, time('00:50')),
(11, 1, time('00:55')),
(12, 1, time('01:00')),
(13, 1, time('01:05')),
(14, 1, time('01:10')),
(15, 1, time('01:15')),
(16, 1, time('01:20')),
(17, 1, time('01:25')),
(18, 1, time('01:30')),
(19, 1, time('01:35')),
(20, 1, time('01:40')),
(21, 1, time('01:45')),
(22, 1, time('01:50')),
(23, 1, time('01:55')),
(24, 1, time('02:00')),
(25, 1, time('02:05')),
(26, 1, time('02:10')),
(27, 1, time('02:15')),
(28, 1, time('02:20')),
(29, 1, time('02:25')),
(30, 1, time('02:30')),
(31, 1, time('02:35')),
(32, 1, time('02:40')),
(33, 1, time('02:45')),
(34, 1, time('02:50')),
(35, 1, time('02:55')),
(36, 1, time('03:00')),
(37, 1, time('03:05')),
(38, 1, time('03:10')),
(39, 1, time('03:15')),
(40, 1, time('03:20')),
(41, 1, time('03:25')),
(42, 1, time('03:30')),
(43, 1, time('03:35')),
(44, 1, time('03:40')),
(45, 1, time('03:45')),
(46, 1, time('03:50')),
(47, 1, time('03:55')),
(48, 1, time('04:00')),
(49, 1, time('04:05')),
(50, 1, time('04:10')),
(51, 1, time('04:15')),
(52, 1, time('04:20')),
(53, 1, time('04:25')),
(54, 1, time('04:30')),
(55, 1, time('04:35')),
(56, 1, time('04:40')),
(57, 1, time('04:45')),
(58, 1, time('04:50')),
(59, 1, time('04:55')),
(60, 1, time('05:00')),
(61, 1, time('05:05')),
(62, 1, time('05:10')),
(63, 1, time('05:15')),
(64, 1, time('05:20')),
(65, 1, time('05:25')),
(66, 1, time('05:30')),
(67, 1, time('05:35')),
(68, 1, time('05:40')),
(69, 1, time('05:45')),
(70, 1, time('05:50')),
(71, 1, time('05:55')),
(72, 1, time('06:00')),
(73, 1, time('06:05')),
(74, 1, time('06:10')),
(75, 1, time('06:15')),
(76, 1, time('06:20')),
(77, 1, time('06:25')),
(78, 1, time('06:30')),
(79, 1, time('06:35')),
(80, 1, time('06:40')),
(81, 1, time('06:45')),
(82, 1, time('06:50')),
(83, 1, time('06:55')),
(84, 1, time('07:00')),
(85, 1, time('07:05')),
(86, 1, time('07:10')),
(87, 1, time('07:15')),
(88, 1, time('07:20')),
(89, 1, time('07:25')),
(90, 1, time('07:30')),
(91, 1, time('07:35')),
(92, 1, time('07:40')),
(93, 1, time('07:45')),
(94, 1, time('07:50')),
(95, 1, time('07:55')),
(96, 1, time('08:00')),
(97, 1, time('08:05')),
(98, 1, time('08:10')),
(99, 1, time('08:15')),
(100, 1, time('08:20')),
(101, 1, time('08:25')),
(102, 1, time('08:30')),
(103, 1, time('08:35')),
(104, 1, time('08:40')),
(105, 1, time('08:45')),
(106, 1, time('08:50')),
(107, 1, time('08:55')),
(108, 1, time('09:00')),
(109, 1, time('09:05')),
(110, 1, time('09:10')),
(111, 1, time('09:15')),
(112, 1, time('09:20')),
(113, 1, time('09:25')),
(114, 1, time('09:30')),
(115, 1, time('09:35')),
(116, 1, time('09:40')),
(117, 1, time('09:45')),
(118, 1, time('09:50')),
(119, 1, time('09:55')),
(120, 1, time('10:00')),
(121, 1, time('10:05')),
(122, 1, time('10:10')),
(123, 1, time('10:15')),
(124, 1, time('10:20')),
(125, 1, time('10:25')),
(126, 1, time('10:30')),
(127, 1, time('10:35')),
(128, 1, time('10:40')),
(129, 1, time('10:45')),
(130, 1, time('10:50')),
(131, 1, time('10:55')),
(132, 1, time('11:00')),
(133, 1, time('11:05')),
(134, 1, time('11:10')),
(135, 1, time('11:15')),
(136, 1, time('11:20')),
(137, 1, time('11:25')),
(138, 1, time('11:30')),
(139, 1, time('11:35')),
(140, 1, time('11:40')),
(141, 1, time('11:45')),
(142, 1, time('11:50')),
(143, 1, time('11:55')),
(144, 1, time('12:00')),
(145, 1, time('12:05')),
(146, 1, time('12:10')),
(147, 1, time('12:15')),
(148, 1, time('12:20')),
(149, 1, time('12:25')),
(150, 1, time('12:30')),
(151, 1, time('12:35')),
(152, 1, time('12:40')),
(153, 1, time('12:45')),
(154, 1, time('12:50')),
(155, 1, time('12:55')),
(156, 1, time('13:00')),
(157, 1, time('13:05')),
(158, 1, time('13:10')),
(159, 1, time('13:15')),
(160, 1, time('13:20')),
(161, 1, time('13:25')),
(162, 1, time('13:30')),
(163, 1, time('13:35')),
(164, 1, time('13:40')),
(165, 1, time('13:45')),
(166, 1, time('13:50')),
(167, 1, time('13:55')),
(168, 1, time('14:00')),
(169, 1, time('14:05')),
(170, 1, time('14:10')),
(171, 1, time('14:15')),
(172, 1, time('14:20')),
(173, 1, time('14:25')),
(174, 1, time('14:30')),
(175, 1, time('14:35')),
(176, 1, time('14:40')),
(177, 1, time('14:45')),
(178, 1, time('14:50')),
(179, 1, time('14:55')),
(180, 1, time('15:00')),
(181, 1, time('15:05')),
(182, 1, time('15:10')),
(183, 1, time('15:15')),
(184, 1, time('15:20')),
(185, 1, time('15:25')),
(186, 1, time('15:30')),
(187, 1, time('15:35')),
(188, 1, time('15:40')),
(189, 1, time('15:45')),
(190, 1, time('15:50')),
(191, 1, time('15:55')),
(192, 1, time('16:00')),
(193, 1, time('16:05')),
(194, 1, time('16:10')),
(195, 1, time('16:15')),
(196, 1, time('16:20')),
(197, 1, time('16:25')),
(198, 1, time('16:30')),
(199, 1, time('16:35')),
(200, 1, time('16:40')),
(201, 1, time('16:45')),
(202, 1, time('16:50')),
(203, 1, time('16:55')),
(204, 1, time('17:00')),
(205, 1, time('17:05')),
(206, 1, time('17:10')),
(207, 1, time('17:15')),
(208, 1, time('17:20')),
(209, 1, time('17:25')),
(210, 1, time('17:30')),
(211, 1, time('17:35')),
(212, 1, time('17:40')),
(213, 1, time('17:45')),
(214, 1, time('17:50')),
(215, 1, time('17:55')),
(216, 1, time('18:00')),
(217, 1, time('18:05')),
(218, 1, time('18:10')),
(219, 1, time('18:15')),
(220, 1, time('18:20')),
(221, 1, time('18:25')),
(222, 1, time('18:30')),
(223, 1, time('18:35')),
(224, 1, time('18:40')),
(225, 1, time('18:45')),
(226, 1, time('18:50')),
(227, 1, time('18:55')),
(228, 1, time('19:00')),
(229, 1, time('19:05')),
(230, 1, time('19:10')),
(231, 1, time('19:15')),
(232, 1, time('19:20')),
(233, 1, time('19:25')),
(234, 1, time('19:30')),
(235, 1, time('19:35')),
(236, 1, time('19:40')),
(237, 1, time('19:45')),
(238, 1, time('19:50')),
(239, 1, time('19:55')),
(240, 1, time('20:00')),
(241, 1, time('20:05')),
(242, 1, time('20:10')),
(243, 1, time('20:15')),
(244, 1, time('20:20')),
(245, 1, time('20:25')),
(246, 1, time('20:30')),
(247, 1, time('20:35')),
(248, 1, time('20:40')),
(249, 1, time('20:45')),
(250, 1, time('20:50')),
(251, 1, time('20:55')),
(252, 1, time('21:00')),
(253, 1, time('21:05')),
(254, 1, time('21:10')),
(255, 1, time('21:15')),
(256, 1, time('21:20')),
(257, 1, time('21:25')),
(258, 1, time('21:30')),
(259, 1, time('21:35')),
(260, 1, time('21:40')),
(261, 1, time('21:45')),
(262, 1, time('21:50')),
(263, 1, time('21:55')),
(264, 1, time('22:00')),
(265, 1, time('22:05')),
(266, 1, time('22:10')),
(267, 1, time('22:15')),
(268, 1, time('22:20')),
(269, 1, time('22:25')),
(270, 1, time('22:30')),
(271, 1, time('22:35')),
(272, 1, time('22:40')),
(273, 1, time('22:45')),
(274, 1, time('22:50')),
(275, 1, time('22:55')),
(276, 1, time('23:00')),
(277, 1, time('23:05')),
(278, 1, time('23:10')),
(279, 1, time('23:15')),
(280, 1, time('23:20')),
(281, 1, time('23:25')),
(282, 1, time('23:30')),
(283, 1, time('23:35')),
(284, 1, time('23:40')),
(285, 1, time('23:45')),
(286, 1, time('23:50')),
(287, 1, time('23:55'))
;
