var logger = require('../../self_modules/logger.js');

logger.system.info("test logging to system");
logger.system.fatal("test logging to system");
logger.system.error("test logging to system");
logger.system.warn("test logging to system");
logger.system.debug("test logging to system");
logger.tweet.info("test logging to tweet");
logger.school_bus.info("test logging to school_bus");
logger.kikaku.info("test logging to kikaku");
logger.shop.info("test logging to shop");
