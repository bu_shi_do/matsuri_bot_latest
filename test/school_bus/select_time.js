var selectTime = require('../../school_bus_bot/modules/new_select_time.js');
var timeTableDb = require('../../school_bus_bot/modules/schoolbus_model.js');

var timeTable;

setTimeout(function() {
    timeTable = timeTableDb.getTimeTable('kyutech');
    var now = new Date();
    console.log('now: ' + now);
    console.log(selectTime.selectFromTimeTable(now.getTime(), timeTable.time));
}, 100);
