var twitter = require('twitter');
//var Promise = require('bluebird');
var logger = require('./../../self_modules/newlogger');
var key = require('./../../json/bot_key.json');
var auto_reply = require('./../../school_bus_bot/auto_reply_kai');

var BOT_ID = 2787995245;

var bot = new twitter({
	consumer_key 		:key.consumerKey,
	consumer_secret 	:key.consumerSecret,
	access_token_key	:key.accessTokenKey,
	access_token_secret :key.accessTokenSecret
});

logger.system.info('System started');

bot.stream('user', function(stream){
	stream.on('data', function(data){
		if(!('text' in data)){ return; }
		//var screen_name = data.user.screen_name;
		//var user_name = data.user.name;
		var user_id = data.user.id_str;
		var twtext = data.text;

		var isRetweet = data.retweeted_status ? true : false;	// 公式RTかどうか
		var isRetweet2 = (new RegExp(/(R|Q)T @[^\s　]+/g)).test(twtext);	// 非公式RT,QTかどうか
		var isLink = data.entities.urls.length ? true : false;	// リンクを含むかどうか

		twtext = twtext.replace(new RegExp(/@[^\s　]+/g),'');	// @aaaaaは全て除外
		twtext = twtext.replace(/[\n\r]/g,"");	//改行を除去

		/* 自分自身のツイートは除外 */
		if(user_id === BOT_ID){ return; }
		/* リンク付きとRTは除外 */
		if(isRetweet || isRetweet2 || isLink){ return; }


		auto_reply.auto_reply(data);
	});
	stream.on('error', function(error) {
        console.log(error);
//		throw error;
	});
    stream.on('end', function() {
    });
});
