use matsuri;

CREATE TABLE shop (
  id int PRIMARY KEY,
  name varchar(128),
  group_name_id int,
  booth int,
  INDEX(group_name_id)
) CHARACTER SET 'utf8';

CREATE TABLE group_name (
  id int,
  name varchar(128),
  PRIMARY KEY(id)
) CHARACTER SET 'utf8';

CREATE TABLE group_another (
  id int,
  group_name_id int,
  name varchar(128),
  PRIMARY KEY(id)
) CHARACTER SET 'utf8';

CREATE TABLE exhibits (
  id int,
  shop_id int,
  goods_id int,
  PRIMARY KEY(id),
  INDEX(shop_id),
  INDEX(goods_id)
);

CREATE TABLE goods (
  id int,
  name varchar(128),
  PRIMARY KEY(id)
) CHARACTER SET 'utf8';

CREATE TABLE goods_another (
  id int,
  goods_id int,
  name varchar(128),
  PRIMARY KEY(id)
) CHARACTER SET 'utf8';
