USE matsuri;

CREATE TABLE bus_stop (
    id int not null,
    name varchar(100) not null,
    PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE bus_timetable (
    id int not null,
    bus_stop_id int,
    depart_time time,
    PRIMARY KEY (id)
);
