DROP TABLE IF EXISTS project;
DROP TABLE IF EXISTS location;
DROP TABLE IF EXISTS project_word;

CREATE TABLE project (
    id int PRIMARY KEY,
    start_time datetime,
    end_time datetime,
    edited_start_time datetime,
    edited_end_time datetime,
    title varchar(128),
    location_id int,
    introduction varchar(255),
    tweet_interval int
);

CREATE TABLE location (
    id int,
    name varchar(128),
    PRIMARY KEY (id)
);

CREATE TABLE project_word (
    id int,
    project_id int,
    word varchar(255),
    PRIMARY KEY (id)
);


INSERT INTO location (id, name) VALUES
(1, '多目的グラウンド内特設野外ステージ'),
(2, '体育館内特設ステージ');

INSERT INTO project (id, start_time, end_time, edited_start_time, edited_end_time, title, location_id, introduction, tweet_interval) VALUES
(1, datetime('2015-11-16 00:00'), datetime('2015-11-16 23:56'), 0, 0, '開催中ツイートてすと',1, '開催中', 1),
(2, datetime('2015-11-15 18:00'), datetime('2015-11-15 19:00'), 0, 0, 'ピルエット',1, 'ジャグリングショーを行います', 1);
