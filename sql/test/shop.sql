use matsuri;

INSERT INTO shop (id, name, group_name_id, booth) VALUES (1, "さらおわ", 1, 1);
INSERT INTO shop (id, name, group_name_id, booth) VALUES (2, "新しいfolder", 2, 2);

INSERT INTO group_name (id, name) VALUES (1, "自治ネットワーク委員会");
INSERT INTO group_name (id, name) VALUES (2, "C3");

INSERT INTO group_another (id, group_name_id, name) VALUES (1, 1, "自治ネットワーク委員会");
INSERT INTO group_another (id, group_name_id, name) VALUES (2, 1, "自治ね");
INSERT INTO group_another (id, group_name_id, name) VALUES (3, 2, "C3");
INSERT INTO group_another (id, group_name_id, name) VALUES (4, 2, "シースリー");

INSERT INTO goods (id, name) VALUES (1, "揚げアイス");
INSERT INTO goods (id, name) VALUES (2, "うどん");
INSERT INTO goods (id, name) VALUES (3, "飲み物");

INSERT INTO goods_another (id, goods_id, name) VALUES (1, 1, "揚げアイス");
INSERT INTO goods_another (id, goods_id, name) VALUES (2, 1, "アイス");
INSERT INTO goods_another (id, goods_id, name) VALUES (3, 2, "うどん");
INSERT INTO goods_another (id, goods_id, name) VALUES (4, 3, "飲み物");
INSERT INTO goods_another (id, goods_id, name) VALUES (5, 4, "ジュース");
INSERT INTO goods_another (id, goods_id, name) VALUES (6, 5, "ソフトドリンク");

INSERT INTO exhibits (id, shop_id, goods_id) VALUES (1,1,1);
INSERT INTO exhibits (id, shop_id, goods_id) VALUES (2,2,2);
INSERT INTO exhibits (id, shop_id, goods_id) VALUES (3,2,3);