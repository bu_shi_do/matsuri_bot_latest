USE matsuri;

INSERT INTO location (id, name) VALUES (1, '場所1');
INSERT INTO location (id, name) VALUES (2, '場所2');
INSERT INTO location (id, name) VALUES (3, '場所3');

INSERT INTO project_word (id, project_id, word) VALUES (1, 1, 'テスト1');
INSERT INTO project_word (id, project_id, word) VALUES (2, 1, 'テスト1-1');
INSERT INTO project_word (id, project_id, word) VALUES (3, 2, 'テスト2');
INSERT INTO project_word (id, project_id, word) VALUES (4, 2, 'テスト2-2');

INSERT INTO project (id, start_time, end_time, edited_start_time, edited_end_time, title, location_id, introduction, tweet_interval) VALUES (1, '2015-08-30 10:00:00', '2015-08-30 12:00:00', null, null, 'テストイベント1', 1, '説明テスト1', 30); 
INSERT INTO project (id, start_time, end_time, edited_start_time, edited_end_time, title, location_id, introduction, tweet_interval) VALUES (2, '2015-08-30 13:00:00', '2015-08-30 16:00:00', null, null, 'テストイベント2', 2, '説明テスト2', 30); 
